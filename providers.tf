provider "google" {
  credentials = file("./creds/serviceaccount.json")
  project     = "burnished-case-280710"
  region      = "us-central1"
  zone        = "us-central1-a"
}

